<?php

namespace Drupal\entity_search_autoindex\Helper;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\TypedData\EntityDataDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceDefinitionInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\entity_search_autoindex\BundleFormTrait;
use Drupal\facets\Processor\ProcessorPluginManager;
use Drupal\facets\Widget\WidgetPluginManager;
use Drupal\search_api\IndexBatchHelper;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Processor\ProcessorPropertyInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api\Utility\DataTypeHelperInterface;
use Drupal\search_api\Utility\FieldsHelperInterface;
use Drupal\search_api\Utility\PluginHelperInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The helper class for the entity form altering and other Search API processes.
 */
class BundleFormHelper {

  use StringTranslationTrait;
  use BundleFormTrait;
  use MessengerTrait;
  use LoggerChannelTrait;

  /**
   * Keep class object.
   *
   * @var object
   */
  public static ?object $helper = NULL;

  /**
   * Protected container variable.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected ContainerInterface $container;

  /**
   * Protected configFactory variable.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * Protected entityTypeManager variable.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Protected request variable.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * The plugin helper.
   *
   * @var \Drupal\search_api\Utility\PluginHelperInterface
   */
  protected PluginHelperInterface $pluginHelper;

  /**
   * The data type helper.
   *
   * @var \Drupal\search_api\Utility\DataTypeHelperInterface
   */
  protected DataTypeHelperInterface $dataTypeHelper;

  /**
   * The fields helper.
   *
   * @var \Drupal\search_api\Utility\FieldsHelperInterface
   */
  protected FieldsHelperInterface $fieldsHelper;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The plugin manager for processors.
   *
   * @var \Drupal\facets\Processor\ProcessorPluginManager
   */
  protected ProcessorPluginManager $facetsProcessorPluginManager;

  /**
   * The plugin manager for widgets.
   *
   * @var \Drupal\facets\Widget\WidgetPluginManager
   */
  protected WidgetPluginManager $widgetPluginManager;

  /**
   * BundleFormHelper constructor.
   */
  protected function __construct() {
    $this->container = \Drupal::getContainer();
    $this->configFactory = $this->container->get('config.factory');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->request = $this->container->get('request_stack')->getCurrentRequest();
    $this->pluginHelper = $this->container->get('search_api.plugin_helper');
    $this->dataTypeHelper = $this->container->get('search_api.data_type_helper');
    $this->fieldsHelper = $this->container->get('search_api.fields_helper');
    $this->entityDisplayRepository = $this->container->get('entity_display.repository');
    $this->facetsProcessorPluginManager = $this->container->get('plugin.manager.facets.processor');
    $this->widgetPluginManager = $this->container->get('plugin.manager.facets.widget');
  }

  /**
   * Get the class instance using this function.
   *
   * @return \Drupal\entity_search_autoindex\Helper\BundleFormHelper
   *   Form helper.
   */
  public static function getInstance(): BundleFormHelper {
    if (!self::$helper) {
      self::$helper = new BundleFormHelper();
    }
    return self::$helper;
  }

  /**
   * Alter the entity form callback.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $form_id
   *   Form id.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, string $form_id) {
    $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
    $entity_type = $entity->getEntityType()->getBundleOf();
    $bundle = $entity->id();
    $index_name = $bundle ? $this->getMachineName('index', $entity_type, $bundle) : NULL;

    /** @var \Drupal\search_api\IndexInterface $index */
    $index = $bundle ? $this->entityTypeManager->getStorage('search_api_index')->load($index_name) : '';

    // A basic validation if there are no any Search API servers.
    if (!$this->getServerOptions()) {
      $form['create_index_help'] = [
        '#type' => 'markup',
        '#markup' => $this->t('
        <strong>NOTE:</strong> You can not create any <em>Auto Index</em> until
        there will be any Search API server setup. In order to create one, you
        can go <a href=":link">@here</a>.', [
          '@here' => $this->t('here'),
          ':link' => Url::fromRoute('entity.search_api_server.add_form')->toString(),
        ]),
      ];
      return;
    }

    $form['create_index'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create search api index.'),
      '#default_value' => (bool) $index,
      '#disabled' => !$bundle || $index,
    ];

    if (!$bundle) {
      $form['create_index']['#description'] = $this->t('Search api index settings will be available after save.');
      return;
    }

    $form['csa_index'] = [
      '#type' => 'details',
      '#title' => $this->t('Search api index settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name=create_index]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['csa_index']['server'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Server'),
      '#description' => $this->t('Select the server this index should use. Indexes cannot be enabled without a connection to a valid, enabled server.'),
      '#options' => $this->getServerOptions(),
      '#default_value' => ($index && $index->hasValidServer()) ? $index->getServerId() : '',
    ];

    $form['csa_index']['immediate_reindex'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Start indexing after save'),
      '#description' => $this->t('If checked, it will trigger the re-index after
      this entity form save.'),
      '#default_value' => FALSE,
    ];

    $form['csa_index']['fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Fields to include in index'),
      '#description' => $this->t('<strong>NOTE:</strong> There is no possibility
      to uncheck none of the fields because this configuration form does not
      manipulate the <em>Search API</em> index, fields, facets, or blocks
      deletion but only the creation. If any of the field gets manually removed,
      there will be the possibiltiy to check it back again.'),
      '#open' => TRUE,
      '#tree' => FALSE,
    ];

    if (!$index) {
      $index_defaults = [
        'name' => 'Auto created for ' . $entity_type . ' ' . $bundle,
        'id' => $index_name,
        'status' => 1,
      ];
      /** @var \Drupal\search_api\IndexInterface $index */
      $index = $this->entityTypeManager->getStorage('search_api_index')->create($index_defaults);
      $datasource_configs['entity:' . $entity_type] = [
        'bundles' => [
          'default' => 0,
          'selected' => [$bundle],
        ],
        'languages' => [
          'default' => 1,
          'selected' => [],
        ],
      ];
      $datasources = $this->pluginHelper->createDatasourcePlugins($index, ['entity:' . $entity_type], $datasource_configs);
      $index->setDatasources($datasources);
    }
    $form['csa_index']['fields'] += $this->getProperties($index);

    $form['csa_index']['facet_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Facets to generate for the view.'),
      '#open' => TRUE,
      '#tree' => FALSE,
    ];

    $facet_fields = [];
    $default_facet_fields = [];
    foreach ($index->getFields() as $field_key => $facet_field) {
      $identifier = $facet_field->getFieldIdentifier();
      $facet_fields[$field_key] = $facet_field->getLabel() . ': (' . $identifier . ')';

      $facet_name = $this->getMachineName('facet', $entity_type . '__' . $bundle, $identifier);
      // Load the facet by the name and populate the default enabled facets
      // options (if any).
      /** @var \Drupal\facets\FacetInterface $facet */
      $facet = $this->entityTypeManager->getStorage('facets_facet')->load($facet_name);
      $default_facet_widget = 'checkbox';
      if ($facet) {
        $default_facet_fields[] = $field_key;
        $default_facet_widget = $facet->getWidget()['type'];
      }

      $widget_options = [];
      foreach ($this->widgetPluginManager->getDefinitions() as $widget_id => $definition) {
        $widget_options[$widget_id] = !empty($definition['label']) ? $definition['label'] : $widget_id;
      }

      $active_facet_field = isset($facet) ? TRUE : FALSE;
      $form['csa_index']['facet_fields']['facet_fields'][$field_key] = [
        '#type' => 'checkbox',
        '#parents' => ['facet_fields', $field_key],
        '#title'  => $facet_field->getLabel() . ': (' . $identifier . ')',
        '#default_value' => $active_facet_field,
        '#disabled' => $active_facet_field,
      ];
      $form['csa_index']['facet_fields']['facet_fields']["widget__$field_key"] = [
        '#type' => 'radios',
        '#title' => $this->t('Facet widget'),
        '#attributes' => [
          'style' => 'padding-left: 20px;',
        ],
        '#parents' => ['facet_fields', "widget__$field_key"],
        '#options' => $widget_options,
        '#default_value' => $default_facet_widget,
        '#disabled' => $active_facet_field ? TRUE : FALSE,
      ];
    }

    if (!$facet_fields) {
      $form['csa_index']['facet_fields']['markup'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<strong>NOTE:</strong> In order to setup a facet
        field, there should be be configured Search API index field. To afford
        that, submit the form with the pre-selected fields then, return back and
        configure the desired facets and blocks.'),
      ];
    }

    $view_name = $bundle ? $this->getMachineName('view', $entity_type, $bundle) : NULL;
    /** @var \Drupal\views\ViewEntityInterface $view */
    $view = $view_name ? $this->entityTypeManager->getStorage('view')->load($view_name) : NULL;
    $path_path = ['page_' . $view_name, 'display_options', 'path'];
    $view_display = $view ? $view->get('display') : NULL;
    $path_current = $view ? NestedArray::getValue($view_display, $path_path) : '/' . $entity_type . '/' . $bundle;
    $view_mode_path = [
      'default',
      'display_options',
      'row',
      'options',
      'view_modes',
      'entity:' . $entity_type,
      $bundle,
    ];
    $view_mode_current = $view ? NestedArray::getValue($view_display, $view_mode_path) : 'default';
    $label_current = $view ? NestedArray::getValue($view_display, [
      'default',
      'display_options',
      'title',
    ]
    ) : ucfirst($entity_type . ' ' . $bundle);

    $form['create_view'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create index based view.'),
      '#default_value' => (bool) $view,
      '#disabled' => !$bundle || $view,
      '#states' => [
        'visible' => [
          ':input[name=create_index]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['csa_view'] = [
      '#type' => 'details',
      '#title' => $this->t('Search api view settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name=create_view]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['csa_view']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('View path'),
      '#default_value' => $path_current,
    ];

    $form['csa_view']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('View title'),
      '#default_value' => $label_current,
    ];

    $view_mode_options = ['default' => $this->t('Default')];
    $view_modes = $this->entityDisplayRepository->getViewModes($entity_type);
    foreach ($view_modes as $view_mode_id => $view_mode) {
      $view_mode_options[$view_mode_id] = $view_mode['label'];
    }
    $form['csa_view']['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity display mode'),
      '#default_value' => $view_mode_current,
      '#options' => $view_mode_options,
    ];

    $form['csa_view']['create_facets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create facet blocks based on reference fields from index.'),
      '#default_value' => TRUE,
      '#disabled' => TRUE,
    ];

    // Get the system default themes regions.
    $active_theme = $this->configFactory->get('system.theme')->get('default');
    $regions = system_region_list($active_theme);

    $form['csa_view']['facet_block_region'] = [
      '#type' => 'select',
      '#title' => $this->t('Facet blocks theme region'),
      '#description' => $this->t('Set a theme region of where to place the facet
      blocks. NOTE: The regions are loaded from the default system theme.'),
      '#options' => $regions,
    ];

    $form['actions']['submit']['#submit'][] = __CLASS__ . '::submit';
  }

  /**
   * Submit entity bundle form.
   *
   * @param array $form
   *   Form state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public static function submit(array &$form, FormStateInterface $form_state) {
    self::getInstance()->submitForm($form, $form_state);
  }

  /**
   * Submit entity bundle form.
   *
   * @param array $form
   *   Form state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('create_index'))) {
      return;
    }

    $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
    $entity_type = $entity->getEntityType()->getBundleOf();
    $bundle = $entity->id();
    $index_name = $bundle ? $this->getMachineName('index', $entity_type, $bundle) : NULL;

    $index_defaults = [
      'name' => 'Auto created for ' . $entity_type . ' ' . $bundle,
      'id' => $index_name,
      'status' => 1,
    ];

    /** @var \Drupal\search_api\IndexInterface $index */
    $index = $this->entityTypeManager->getStorage('search_api_index')->load($index_name);
    if (!$index) {
      /** @var \Drupal\search_api\IndexInterface $index */
      $index = $this->entityTypeManager->getStorage('search_api_index')->create($index_defaults);
    }

    $default_options = ['index_directly' => 1, 'cron_limit' => 50];
    $index->setOptions($default_options + $index->getOptions());

    $datasource_configs['entity:' . $entity_type] = [
      'bundles' => [
        'default' => 0,
        'selected' => [$bundle],
      ],
      'languages' => [
        'default' => 1,
        'selected' => [],
      ],
    ];
    $datasources = $this->pluginHelper->createDatasourcePlugins($index, ['entity:' . $entity_type], $datasource_configs);
    $index->setDatasources($datasources);

    $tracker = $this->pluginHelper->createTrackerPlugin($index, $index->getTrackerId(), ['indexing_order' => 'fifo']);
    $index->setTracker($tracker);

    $server_id = $form_state->getValue(['csa_index', 'server']);
    $server = $this->entityTypeManager->getStorage('search_api_server')->load($server_id);
    $index->setServer($server);

    $index_fields = $index->getFields();
    $current_index_fields = [];
    foreach ($index_fields as $index_field) {
      $current_index_fields[$index_field->getPropertyPath()] = $index_field->getPropertyPath();
    }

    // Here we add all the configured field into the index.
    $fields = $form_state->getValue(['csa_index', 'fields']);
    foreach ($fields as $datasource_key => $list) {
      $filtered = array_filter($list);
      foreach (array_keys($filtered) as $path) {
        $expanded_path = explode(':', str_replace(':', ':properties:', $path));
        array_unshift($expanded_path, 'csa_index', 'fields', $datasource_key);
        $expanded_path[] = $path;
        $form_element = NestedArray::getValue($form, [] + $expanded_path);

        if (isset($current_index_fields[$path])) {
          continue;
        }
        $field = $this->fieldsHelper->createFieldFromProperty($index, $form_element['#property'], $form_element['#datasource_id'], $path, NULL, $form_element['#data_type']);
        $field->setLabel($form_element['#prefixed_label']);
        $index->addField($field);
      }
    }
    $index->save();
    $this->messenger()->addStatus($this->t('The search api index was saved.'));

    try {
      $index->clear();
      $index->reindex();
      $this->messenger->addStatus($this->t('The search index %name was successfully queued for reindexing.', ['%name' => $index->label()]));
    }
    catch (SearchApiException $e) {
      $this->messenger->addError($this->t('Failed to queue items for reindexing on search index %name.', ['%name' => $index->label()]));
      $message = '%type while trying to queue items for reindexing on index %name: @message in %function (line %line of %file)';
      $variables = [
        '%name' => $index->label(),
      ];
      $variables += Error::decodeException($e);
      $this->getLogger('search_api')->error($message, $variables);
    }

    if ($form_state->getValue(['csa_index', 'immediate_reindex'])) {
      try {
        IndexBatchHelper::setStringTranslation($this->getStringTranslation());
        IndexBatchHelper::create($index, $default_options['cron_limit'], -1);
      }
      catch (SearchApiException $e) {
        $this->messenger->addWarning($this->t('Failed to create a batch, please check the batch size and limit.'));
      }
    }

    // Now, we create the view basec on the configured index.
    if ($form_state->getValue(['create_view'])) {
      $view_path = ltrim($form_state->getValue(['csa_view', 'path']), '/');
      $view_label = trim($form_state->getValue(['csa_view', 'title']));
      $view_mode = $form_state->getValue(['csa_view', 'view_mode']);

      $view = $this->createView($view_path, $view_label, $view_mode, $entity_type, $bundle, $index_name);
      $this->messenger()->addStatus($this->t('The Index based view was saved with success.'));
    }

    if ($form_state->getValue(['create_view']) && $form_state->getValue(['csa_view', 'create_facets'])) {
      $view_name = $bundle ? $this->getMachineName('view', $entity_type, $bundle) : NULL;
      // Facets do not work with the cached views.
      $display = $view->get('display');
      $cache_path = ['default', 'display_options', 'cache', 'type'];
      if (NestedArray::getValue($display, $cache_path) != 'none') {
        NestedArray::setValue($display, $cache_path, 'none');
        $view->set('display', $display);
        $view->save();
      }

      $facet_fields = array_filter((array) $form_state->getValue('facet_fields'));

      // Here, we add all the configured facets. Based on the created facets
      // we're going to add the blocks and place in the configured theme region.
      $active_facet_fields = [];
      // Loop all the available index fields and filter by the selected ones.
      foreach ($index->getFields() as $field_key => $field) {
        if (!in_array($field_key, array_keys($facet_fields))) {
          continue;
        }
        $active_facet_fields[$field->getPropertyPath()] = [
          'field' => $field,
          'widget' => $facet_fields["widget__$field_key"],
        ];
      }

      $facet_processors = $this->getFacetsProcessors();
      // Loop all the configured fields and try to generate facets from them.
      foreach ($active_facet_fields as $field_path => $field_config) {
        $identifier = $field_config['field']->getFieldIdentifier();
        $label = $field_config['field']->getLabel();

        $facet_name = $this->getMachineName('facet', $entity_type . '__' . $bundle, $identifier);
        $facet = $this->entityTypeManager->getStorage('facets_facet')->load($facet_name);

        // First of all, try to fetch the facet (if exists).
        if (!$facet) {
          $defaults = [
            'id' => $facet_name,
            'name' => 'Facet >> ' . $bundle . ' >> ' . $label,
          ];
          /** @var \Drupal\facets\FacetInterface $facet */
          $facet = $this->entityTypeManager->getStorage('facets_facet')->create($defaults);
          // Here we create and define the new facet config.
          $facet->setFacetSourceId('search_api:views_page__' . $view_name . '__page_' . $view_name);
          $facet->setFieldIdentifier($this->getIndexFieldIdByPath($index, $field_path));

          foreach ($facet_processors as $processor_id => $weights) {
            $facet->addProcessor([
              'processor_id' => $processor_id,
              'weights' => $weights,
              'settings' => [],
            ]);
          }

          $facet->setWidget($field_config['widget']);
          $facet->setUrlAlias(preg_replace('/^field_/', '', $identifier));
          $facet->setWeight(0);
          // Set default empty behaviour.
          $facet->setEmptyBehavior(['behavior' => 'none']);
          $facet->setOnlyVisibleWhenFacetSourceIsVisible(TRUE);
          $facet->save();
          // Add a system message after the facet creation.
          $this->messenger()
            ->addStatus(
              $this->t('Facet for @label (@name) was created.', [
                '@label' => $label,
                '@name' => $identifier,
              ])
            );
        }

        // Get the configured facet block region.
        $block_region = $form_state->getValue('csa_view')['facet_block_region'];
        // Place the block in the configured region.
        $block_id = $this->getMachineName('block', $entity_type . '__' . $bundle, $identifier);
        $active_theme = $this->configFactory->get('system.theme')->get('default');
        $block = $this->getFacetBlock(
          $block_id,
          $facet_name,
          $active_theme,
          $block_region
        );

        if ($block) {
          $block->set('plugin', 'facet_block:' . $facet_name);
          $block->set('settings', [
            'id' => 'facet_block:' . $facet_name,
            'label' => $label,
            'provider' => 'facets',
            'label_display' => 'visible',
            'block_id' => $block_id,
          ] + $block->get('settings'));
          $block->set('visibility', [
            'request_path' => [
              'id' => 'request_path',
              'pages' => '/' . $view_path,
              'negate' => FALSE,
              'context_mapping' => [],
            ],
          ]);
          $block->set(
            'weight',
            (int) preg_replace('/[^\d]/', '', $form_state->getValue([
              'csa_view',
              'category_path',
            ]) ?? ''
          ));
          $block->save();
        }
      }
    }
  }

  /**
   * Retrieves all available Search API servers as an options list.
   *
   * @return array
   *   An associative array mapping server IDs to their labels.
   */
  protected function getServerOptions(): array {
    $options = [];
    /** @var \Drupal\search_api\ServerInterface[] $servers */
    $servers = $this->entityTypeManager->getStorage('search_api_server')->loadMultiple();
    foreach ($servers as $server_id => $server) {
      $options[$server_id] = $server->label();
    }
    return $options;
  }

  /**
   * Retrieves all available properties to be used in index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search api index to get the properties from.
   *
   * @return array
   *   An associative array mapping server properties to their labels.
   */
  protected function getProperties(IndexInterface $index): array {
    $list = [];
    $datasources = [
      '' => NULL,
    ];
    $datasources += $index->getDatasources();

    $selected_paths = [];
    $index_fields = $index->getFields();
    foreach ($index_fields as $field) {
      $selected_paths[$field->getPropertyPath()] = $field->getPropertyPath();
    }

    if (empty($selected_paths)) {
      $list['warning']['#markup'] = '<strong>' . $this->t('WARNING! Index has no fields, recommended fields checked.') . '</strong>';
    }

    foreach ($datasources as $datasource) {
      $datasource_id = $datasource ? $datasource->getPluginId() : NULL;
      $properties = $index->getPropertyDefinitions($datasource_id);
      $group_label = $datasource ? Html::escape((string) $datasource->label()) : (string) $this->t('General');

      if ($properties) {
        $list['datasource_' . $datasource_id] = $this->getPropertiesList($properties, $datasource_id, $selected_paths);
        $list['datasource_' . $datasource_id]['#title'] = $group_label;
      }
    }
    return $list;
  }

  /**
   * Creates an items list for the given properties.
   *
   * @param \Drupal\Core\TypedData\DataDefinitionInterface[] $properties
   *   The property definitions, keyed by their property names.
   * @param string|null $datasource_id
   *   The datasource ID of the listed properties, or NULL for
   *   datasource-independent properties.
   * @param mixed $selected_paths
   *   Selected path.
   * @param string $parent_path
   *   (optional) The common property path prefix of the given properties.
   * @param string $parent_label
   *   Parent label.
   *
   * @return array
   *   A render array representing the given properties and, possibly, nested
   *   properties.
   */
  protected function getPropertiesList(array $properties, $datasource_id, $selected_paths, string $parent_path = '', string $parent_label = '') {
    $list = [];

    $path_segments = explode(':', $parent_path);
    $level = count($path_segments);
    $max_nesting_level_reached = $level == 3;

    $type_mapping = $this->dataTypeHelper->getFieldTypeMapping();

    foreach ($properties as $key => $property) {
      if ($property instanceof ProcessorPropertyInterface && $property->isHidden()) {
        continue;
      }
      $this_path = $parent_path ? $parent_path . ':' : '';
      $this_path .= $key;

      $label = $property->getLabel();
      $prefixed_label = $parent_label . ($parent_label ? ' » ' : '') . $label;
      $property = $this->fieldsHelper->getInnerProperty($property);

      $can_be_indexed = TRUE;
      $nested_properties = [];
      $parent_child_type = NULL;
      if ($property instanceof ComplexDataDefinitionInterface) {
        $can_be_indexed = FALSE;
        $nested_properties = $this->fieldsHelper->getNestedProperties($property);
        $main_property = $property->getMainPropertyName();
        if ($main_property && isset($nested_properties[$main_property])) {
          $parent_child_type = $property->getDataType() . '.';
          $property = $nested_properties[$main_property];
          $parent_child_type .= $property->getDataType();
          unset($nested_properties[$main_property]);
          $can_be_indexed = TRUE;
        }

        // Don't add the additional "entity" property for entity reference
        // fields which don't target a content entity type.
        if (isset($nested_properties['entity'])) {
          $entity_property = $nested_properties['entity'];
          if ($entity_property instanceof DataReferenceDefinitionInterface) {
            $target = $entity_property->getTargetDefinition();
            if ($target instanceof EntityDataDefinitionInterface && !$this->fieldsHelper->isContentEntityType($target->getEntityTypeId())) {
              unset($nested_properties['entity']);
            }
          }
        }
      }

      // Don't allow indexing of properties with unmapped types. Also, prefer
      // a "parent.child" type mapping (taking into account the parent property
      // for, for example, text fields).
      $type = $property->getDataType();
      if ($parent_child_type && !empty($type_mapping[$parent_child_type])) {
        $type = $parent_child_type;
      }
      elseif (empty($type_mapping[$type])) {
        $can_be_indexed = FALSE;
      }

      // If the property can neither be expanded nor indexed, just skip it.
      if (!($nested_properties || $can_be_indexed)) {
        continue;
      }

      $item = [
        '#type' => 'container',
        '#attributes' => [
          'style' => 'padding-left: ' . (20 * $level) . 'px;',
        ],
      ];

      $nested_list = [];
      if ($nested_properties && !$max_nesting_level_reached) {
        $nested_list = $this->getPropertiesList($nested_properties, $datasource_id, $selected_paths, $this_path, $prefixed_label);
      }

      $label_markup = Html::escape($label);
      $escaped_path = Html::escape($this_path);
      $label_markup = "$label_markup <small>(<code>$escaped_path</code>)</small>";

      if ($can_be_indexed) {
        $item[$this_path] = [
          '#type' => 'checkbox',
          '#title' => $label_markup,
          '#property' => $property,
          '#data_type' => $type_mapping[$type],
          '#datasource_id' => $datasource_id,
          '#prefixed_label' => $prefixed_label,
          '#default_value' => isset($selected_paths[$this_path]),
          '#disabled' => isset($selected_paths[$this_path]),
          '#parents' => [
            'csa_index',
            'fields',
            'datasource_' . $datasource_id,
            $this_path,
          ],
        ];
        // Add references.
        $default_keys = [
          // The defaults for any entity type.
          'title',
          'created',
          'changed',
          'langcode',
          // The product varation default (for the commerce setup).
          'variations:entity:price:number',
          'variations:entity:price:currency_code',
        ];
        if (empty($selected_paths) && (preg_match('/.+:entity:name$/', $this_path) || (string) $label == 'ID' || in_array($this_path, $default_keys))) {
          // Provide defaults for new indexes.
          $item[$this_path]['#default_value'] = TRUE;
        }
      }

      if ($nested_list) {
        $item['properties'] = $nested_list;
      }

      $list[$key] = $item;
    }

    return $list;
  }

  /**
   * Get a list of active facet processors.
   *
   * @return array
   *   Returns the list of the facet processors.
   */
  public function getFacetsProcessors(): array {
    $stages = $this->facetsProcessorPluginManager->getProcessingStages();
    $processors_definitions = $this->facetsProcessorPluginManager->getDefinitions();

    $processors = [];

    foreach ($processors_definitions as $processor_id => $processor) {
      $is_locked = isset($processor['locked']) && $processor['locked'] == TRUE;
      $is_default_enabled = isset($processor['default_enabled']) && $processor['default_enabled'] == TRUE;
      if (!($is_locked || $is_default_enabled)) {
        continue;
      }
      $weights = [];
      foreach ($stages as $stage_id => $stage) {
        if (isset($processor['stages'][$stage_id])) {
          $weights[$stage_id] = $processor['stages'][$stage_id];
        }
      }
      $processors[$processor_id] = $weights;
    }
    return $processors;
  }

}
