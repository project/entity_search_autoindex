<?php

namespace Drupal\entity_search_autoindex;

use Drupal\block\BlockInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\FileStorage;
use Drupal\search_api\IndexInterface;
use Drupal\views\ViewEntityInterface;

/**
 * A helper trait used for the BundleFormHelper class.
 */
trait BundleFormTrait {

  /**
   * Sets the string translation service to use.
   *
   * @param string $type
   *   Type of name, can be index or view.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle.
   *
   * @return string
   *   Return concatenated name.
   */
  public function getMachineName(string $type, string $entity_type, string $bundle): string {
    $context = [
      'type' => $type,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
    ];
    $name = 'sa__' . implode('__', $context);

    \Drupal::service('module_handler')->alter('entity_search_autoindex_machine_name', $name, $context);

    return $name;
  }

  /**
   * Get index field id by its path.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index interface.
   * @param string $field_path
   *   The search api field path (usually used for facets).
   *
   * @return string
   *   Returns the search api indes field ID by the given context path.
   */
  public function getIndexFieldIdByPath(IndexInterface $index, string $field_path): string {
    foreach ($index->getFields() as $field_id => $field) {
      if ($field->getPropertyPath() !== $field_path) {
        continue;
      }
      return $field_id;
    }
    return '';
  }

  /**
   * Place a facet block into a region.
   *
   * @param string $block_id
   *   The block ID to load.
   * @param string $facet_name
   *   The search api facet machine name.
   * @param string $theme
   *   The theme machine name (the default theme).
   * @param string $region
   *   The default theme region (of where to place the face block).
   *
   * @return \Drupal\block\BlockInterface
   *   Returns the facet block instance.
   */
  public function getFacetBlock(string $block_id, string $facet_name, string $theme, string $region): BlockInterface {
    $block = \Drupal::entityTypeManager()->getStorage('block')->load($block_id);
    if (!$block) {
      $block = \Drupal::entityTypeManager()->getStorage('block')->create([
        'id' => $block_id,
        'plugin' => 'facet_block:' . $facet_name,
        'region' => $region,
        'theme' => $theme,
        'weight' => -8,
      ]);
      $block->save();
    }
    return $block;
  }

  /**
   * Create the Search API view based on the bundle form configuration context.
   *
   * @param string $view_path
   *   The view routing path.
   * @param string $view_label
   *   The view label/title shown to the enduser.
   * @param string $view_mode
   *   The enttiy view mode to show in the grid view style.
   * @param string $entity_type
   *   The entity type the view will datasource the data from.
   * @param string $bundle
   *   The entity bundle. For ex. "article" for a "node" entity type.
   * @param string $index_name
   *   The Search API index name to target the view.
   *
   * @return \Drupal\views\ViewEntityInterface
   *   Returns the view instance.
   */
  public function createView(string $view_path, string $view_label, string $view_mode, string $entity_type, string $bundle, string $index_name): ViewEntityInterface {
    $display_title = str_replace(' ', '_', 'Page ' . $view_label);
    $view_name = $bundle ? $this->getMachineName('view', $entity_type, $bundle) : NULL;
    /** @var \Drupal\views\ViewEntityInterface $view */
    $view = $view_name ? $this->entityTypeManager->getStorage('view')->load($view_name) : NULL;
    if (!$view) {
      // Get the module path for getting the view template YML file path.
      $view_template = file_get_contents(
        \Drupal::moduleHandler()
          ->getModule('entity_search_autoindex')
          ->getPath() . '/view_template.yml'
      );
      $replacements = [
        '{CSA_INDEX_NAME}' => $index_name,
        '{CSA_VIEW_NAME}' => $view_name,
        '{CSA_VIEW_LABEL}' => $view_label ? $view_label : ucfirst($entity_type . ' ' . $bundle),
        '{CSA_DATA_SOURCE}' => 'entity:' . $entity_type,
        '{CSA_VIEW_MODE}' => $view_mode,
        '{CSA_BUNDLE}' => $bundle,
        '{CSA_VIEW_PATH}' => $view_path,
        '{CSA_DISPLAY_TITLE}' => $display_title,
      ];
      $view_template = strtr($view_template, $replacements);

      $file_storage = new FileStorage(
        \Drupal::moduleHandler()
          ->getModule('entity_search_autoindex')
          ->getPath()
      );
      $view_defaults = $file_storage->decode($view_template);
      /** @var \Drupal\views\ViewEntityInterface $view */
      $view = $this->entityTypeManager->getStorage('view')->create($view_defaults);
      $view->save();
    }
    else {
      $display = $view->get('display');
      $save = FALSE;

      $path_path = ['page_' . $view_name, 'display_options', 'path'];
      $path_current = NestedArray::getValue($display, $path_path);
      if ($path_current != $view_path) {
        NestedArray::setValue($display, $path_path, $view_path);
        $save = TRUE;
      }

      $view_mode_path = [
        'default',
        'display_options',
        'row',
        'options',
        'view_modes',
        'entity:' . $entity_type,
        $bundle,
      ];
      $view_mode_current = NestedArray::getValue($display, $view_mode_path);
      if ($view_mode_current != $view_mode) {
        NestedArray::setValue($display, $view_mode_path, $view_mode);
        $save = TRUE;
      }

      $title_paths = [
        ['default', 'display_options', 'title'],
        ['page_' . $view_name, 'display_options', 'menu', 'title'],
      ];
      foreach ($title_paths as $title_path) {
        $title_current = NestedArray::getValue($display, $title_path);
        if ($title_current != $view_label) {
          NestedArray::setValue($display, $title_path, $view_label);
          $save = TRUE;
        }
      }

      $display_title_path = ['page_' . $view_name, 'display_title'];
      $display_title_current = NestedArray::getValue($display, $display_title_path);
      if ($display_title_current != $display_title) {
        NestedArray::setValue($display, $display_title_path, $display_title);
        $save = TRUE;
      }

      if ($save) {
        $view->set('display', $display);
        $view->save();
      }
    }
    return $view;
  }

}
