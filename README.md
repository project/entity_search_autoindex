CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 The `Entity Search API Autoindex` module is used to automatically generate an
 index and a view with just a few clicks.

 The module alters all the entity edit pages by adding a few checkboxes:
 `Create search api index` and `Create index based view`. Once these 2 options
 are enabled, the site administrator will be able to automatically configure
 a view with the pre-configured list of facets and a new search index for that
 purpose. In other words, there will be an automatically generated search page
 for that configured entity. An entity might be a node, a commerce product
 variation, a user, or any other custom entity.

REQUIREMENTS
-------------------

 * drupal:views
 * drupal:search_api
 * drupal:facets

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/extending-drupal/installing-modules for further
   information.

CONFIGURATION
-------------

 * Open any entity edit admin page. Scroll down and find these 2 checkboxes:
 `Create search api index` and `Create index based view`. Enable them both and
 configure the facets and view.

TROUBLESHOOTING
---------------

 * For error handling, check the `/admin/reports/dblog`, channel `search_api`.


MAINTAINERS
-----------

Current maintainers:

* [Vesterli Andrei](https://www.drupal.org/u/andreivesterli)
